module Main where

import           Data.String.Strip

import           Data.Graph.Inductive.Graph as G (Graph, LPath(..), Node, LNode, Adj(..))
import           Data.Graph.Inductive.Graph (match)

-- Heap
data Heap a b where
  Empty :: Heap a b
  Node :: a -> b -> [Heap a b] -> Heap a b
  deriving (Show)

empty :: Heap a b
empty = Empty

one :: (a, b) -> Heap a b
one (a,b) = Node a b []

insert :: (Ord a) => (a,b) -> Heap a b -> Heap a b
insert e@(a,b) Empty = one e
insert e h           = merge (one e) h

merge :: (Ord a) => Heap a b -> Heap a b -> Heap a b
merge Empty h = h
merge h Empty = h
merge e@(Node a b hs) e'@(Node a' b' hs')
  | a < a' = Node a b (e':hs)
  | otherwise = Node a' b' (e:hs')

mergeAll :: (Ord a) => [Heap a b] -> Heap a b
mergeAll []       = Empty
mergeAll [h]      = h
mergeAll (a:b:hs) = merge (merge a b) (mergeAll hs)

isEmpty :: Heap a b -> Bool
isEmpty Empty = True
isEmpty _     = False

root :: (Ord a) => Heap a b -> Maybe (a, b, Heap a b)
root Empty         = Nothing
root (Node a b hs) = Just (a, b, mergeAll hs)

fromList :: (Ord a) => [(a,b)] -> Heap a b
fromList = foldr insert empty

toList :: (Ord a) => Heap a b -> [(a,b)]
toList Empty         = []
toList (Node a b hs) = (a,b):toList (mergeAll hs)

heapSort :: (Ord a) => [a] -> [a]
heapSort = fmap fst . toList . fromList . fmap (\x->(x,x))

-- Tree
first :: ([a]->Bool) -> [[a]] -> [a]
first f (filter f -> []) = []
first f (filter f -> (x:_)) = x

findP :: G.Node -> [G.LPath a] -> [G.LNode a]
findP _ [] = []
findP n (G.LP []:ps) = findP n ps
findP n (G.LP (p@((v,_):_)):ps)
  | n == v = p
  | otherwise = findP n ps

getDist :: G.Node -> [G.LPath a] -> Maybe a
getDist n (findP n -> []) = Nothing
getDist n (findP n -> (_,d):_) = Just d
-- dikjstra

dijkstra :: forall a b gr . (G.Graph gr, Real b) => Heap b (G.LPath b) -> gr a b -> [G.LPath b]
dijkstra (root -> Just (b, p@(LP ((n, d):ns)), h')) (match n -> (Just (in', _, _, out'), gr)) = p:dijkstra (mergeAll (h':expand d p out')) gr
  where
    expand :: b -> LPath b -> G.Adj b -> [Heap b (G.LPath b)]
    expand d (G.LP p) out' = fmap f out'
      where
        f :: (b, Node) -> Heap b (LPath b)
        f (b, n) = one (d+b, G.LP ((n,d+b):p))
dijkstra (root -> Just (b, p@(LP ((n, d):ns)), h')) (match n -> (Nothing, gr)) = dijkstra h' gr
dijkstra (root -> Nothing) _ = []

spFrom :: (G.Graph gr, Real b) => G.Node -> gr a b -> [G.LPath b]
spFrom n = dijkstra (one (0, G.LP [(n,0)]))

sp :: (G.Graph gr, Real b) => G.Node -> G.Node -> gr a b -> [G.LNode b]
sp a b = reverse . (findP b) . (spFrom a)

main :: IO ()
main = interact strip
